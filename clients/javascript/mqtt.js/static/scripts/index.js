var log       = document.getElementById('log');
var container = document.getElementById('logContainer');
var logItems  = 0;

var socket = io.connect('http://localhost');

var setOptions = function(options) { socket.emit('set',options); };
var startPublishing = function() { socket.emit('start'); };
var stopPublishing = function() { socket.emit('stop'); };

var print = function(type,color,message) {
    var tr = document.createElement('tr');
    var td = tr.appendChild(document.createElement('td'));
    td.innerHTML = type;
    td.style.color = color;
    tr.appendChild(document.createElement('td')).innerHTML = message;
    log.appendChild(tr);
    container.scrollTop = container.scrollHeight;
    if (++logItems > 100) log.rows[0].parentNode.removeChild(log.rows[0]);
};

socket.on('bootstrap',function() { bootstrapUI.apply(this,arguments); });
socket.on('info',function(message) { print('INFO','green',message); });
socket.on('error',function(message) { print('ERROR','red',message); });
socket.on('message',function(message) { print('MSG','orchid',message); });
socket.on('close',function() { socket.close(); });
