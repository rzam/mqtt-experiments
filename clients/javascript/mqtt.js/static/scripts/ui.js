var debounce = null, changedProperties = { };

var FizzyText = function(options) {
    for (var o in options) this[o] = options[o];
    this.start = startPublishing;
    this.stop = stopPublishing;
};

var makeIntegral = function(bar,value) {
    var rounded = Math.round(value);
    if (value != rounded) bar.setValue(rounded);
    if (rounded != bar.last) {
        bar.last = rounded;
        return rounded;
    } else return null;
};

var monitor = function(target) {
    target.onChange(function(value) {
        if (target.property == 'msg/s' || target.property == 'publishers') {
            value = makeIntegral(target,value);
            if (value === null) return;
        }
        if (debounce !== null) clearTimeout(debounce);
        changedProperties[target.property] = value;
        debounce = setTimeout(function() {
            console.log('now');
            setOptions(changedProperties);
            debounce = null;
            changedProperties = { };
        },100);
    });
    return target;
};

var bootstrapUI = function(options) {
    var text     = new FizzyText(options);
    var gui      = new dat.GUI();
    monitor(gui.add(text,'host'));
    monitor(gui.add(text,'port'));
    monitor(gui.add(text,'topic'));
    monitor(gui.add(text,'username'));
    monitor(gui.add(text,'password'));
    monitor(gui.add(text,'publishers',0,10));
    monitor(gui.add(text,'msg/s',0,10));
    monitor(gui.add(text,'subscribe'));
    gui.add(text,'start');
    gui.add(text,'stop');
};
