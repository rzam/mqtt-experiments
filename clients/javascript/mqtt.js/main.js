var path    = require('path');
var open_   = require('open');
var http    = require('http');
var express = require('express');
var socket  = require('socket.io');
var mqtt    = require('mqtt');

var app    = express();
var server = http.Server(app);
var io     = socket(server);

server.listen(6060);

app.use('/',express.static(path.join(__dirname,'static')));
open_('http://localhost:6060');

io.sockets.on('connection',function(socket) {
    socket.on('set',setOptions);
    socket.on('start',start);
    socket.on('stop',stop);
    io.emit('bootstrap',options);
});

process.on('SIGINT',function() {
    io.emit('close');
    server.close();
    process.exit(0);
});

/***** *****/

var options = {
    host       : '192.168.1.29',
    port       : '1883',
    topic      : 'mqtt.js/%',
    username   : 'user',
    password   : 'password',
    publishers : 3,
    'msg/s'    : 1,
    subscribe  : true
};

var started = false;

var publishers = { };
var intervals  = { };
var subscriber = null;

/***** *****/

var start = function() {
    if (started) return;
    started = true;
    startPublishers(options.publishers);
    if (options.subscribe) startSubscriber();
};

var stop = function() {
    if (!started) return;
    checkStopped.skipOnce = true;
    started = false;
    stopPublishers(Infinity);
    stopSubscriber();
};

var setOptions = function(opts) {
    for (var o in opts) options[o] = opts[o];
    if (!started) return;
    if (opts.hasOwnProperty('subscribe')) {
        if (opts.subscribe) startSubscriber();
        else stopSubscriber();
    }
    if (opts.hasOwnProperty('msg/s'))
        restartIntervals();
    if (opts.hasOwnProperty('publishers'))
        resizePublisherPool();
};

/***** *****/

var getRandomString = function() {
    if (!getRandomString.mask)
        getRandomString.mask = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWYZ0123456789';
    var result = '';
    for (var i=0;i<10;++i)
        result += getRandomString.mask[Math.floor(Math.random()*getRandomString.mask.length)];
    return result;
};

var createClient = function(id) {
    var opts;
    var port = parseInt(options.port,10) || 1883;
    if (options.username.length > 0 && options.password.length > 0)
        opts = { clientId: id, username: options.username, password: options.password };
    var client = mqtt.createClient(port,options.host,opts);
    return client;
};

var checkStopped = function() {
    if (Object.keys(publishers).length > 0 || subscriber !== null) return;
    if (checkStopped.skipOnce) {
        delete checkStopped.skipOnce;
        return;
    }
    io.emit('info','Nothing running, stopping...');
    started = false;
};

/***** Publishers *****/

var createPublisher = function(host,port,user,password) {
    var clientID = getRandomString();
    io.emit('info','Starting publisher #' + clientID);
    var publisher = createClient(clientID);
    publisher.on('error',function(error) {
        stopPublisher(publisher,true);
        io.emit('error','Publisher #' + clientID + ' terminated: ' + error);
    });
    return publisher;
};

var startPublishers = function(count) {
    for (var i=0;i<count;++i) {
        var publisher = createPublisher(options.host,options.port,options.username,options.password);
        startPublisherInterval(publisher);
        publishers[publisher.options.clientId] = publisher;
    }
};

var stopPublisher = function(publisher,isError) {
    var id = publisher.options.clientId;
    if (!isError) io.emit('info','Stopping publisher #' + id + '...');
    if (intervals.hasOwnProperty(id)) {
        clearInterval(intervals[id]);
        delete(intervals[id]);
    }
    publishers[id].end();
    delete publishers[id];
    checkStopped();
};

var stopPublishers = function(count) {
    for (var id in publishers) {
        if (--count < 0) return;
        stopPublisher(publishers[id]);
    }
};

var startPublisherInterval = function(publisher) {
    var id = publisher.options.clientId;
    if (intervals.hasOwnProperty(id)) {
        clearInterval(intervals[id]);
        delete intervals[id];
    }
    if (options['msg/s'] === 0) return;
    setTimeout(function() {
        doPublish(publisher,id);
        var interval = setInterval(function() { doPublish(publisher); },Math.floor(1000/options['msg/s']));
        intervals[id] = interval;
    },Math.floor(Math.random()*1000));
};

var restartIntervals = function() {
    for (var id in intervals)
        startPublisherInterval(publishers[id]);
};

var doPublish = function(publisher) {
    publisher.publish(options.topic.replace(/%/g,publisher.options.clientId),getRandomString());
};

var resizePublisherPool = function() {
    var current = Object.keys(publishers).length;
    stopPublishers(current - options.publishers);
    startPublishers(options.publishers - current);
};

/***** Subscribers *****/

var startSubscriber = function() {
    if (subscriber !== null) return;
    io.emit('info','Starting subscriber...');
    subscriber = createClient('mqtt.js.subscriber');
    subscriber.subscribe(options.topic.replace(/%/g,'#'));
    subscriber.on('error',function(error) {
        io.emit('error','Subscriber terminated: ' + error);
        stopSubscriber(true);
    });
    subscriber.on('message',function(topic,message) {
        io.emit('message','[' + topic + '] ' + message);
    });
};

var stopSubscriber = function(isError) {
    if (subscriber === null) return;
    if (!isError) io.emit('info','Stopping subscriber...');
    subscriber.end();
    subscriber = null;
    checkStopped();
};
