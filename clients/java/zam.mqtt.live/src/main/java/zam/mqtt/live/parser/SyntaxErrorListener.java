package zam.mqtt.live.parser;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

public class SyntaxErrorListener extends BaseErrorListener {

	private String lastMessage;
	private long lastMessageTimestamp = 0;
	
	@Override
	public void syntaxError(Recognizer<?, ?> recognizer,
			Object offendingSymbol, int line, int charPositionInLine,
			String msg, RecognitionException e) {
		long now = System.currentTimeMillis();
		if (now-lastMessageTimestamp < 100) lastMessage += "\n" + msg;
		else lastMessage = msg;
		lastMessageTimestamp = now;
	}
	
	public String getLastMessage() {
		return lastMessage;
	}
	
}