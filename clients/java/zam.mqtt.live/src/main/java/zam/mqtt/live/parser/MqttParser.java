package zam.mqtt.live.parser;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

public class MqttParser {
	
	private SyntaxErrorListener listener = new SyntaxErrorListener();
	private MqttConcreteLiveVisitor visitor = new MqttConcreteLiveVisitor();

	public ParseData parse(String input) {
		ANTLRInputStream inputStream = new ANTLRInputStream(input);
		MqttLiveLexer lexer = new MqttLiveLexer(inputStream);
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		MqttLiveParser parser = new MqttLiveParser(tokenStream);
		lexer.removeErrorListeners();
		parser.removeErrorListeners();
		lexer.addErrorListener(listener);
		parser.addErrorListener(listener);
		//parser.setErrorHandler(new BailErrorStrategy());
		ParseData result = null;
		try { result = visitor.visit(parser.root()); }
		catch (Exception e) { }
		if (result == null)
			throw new RuntimeException(listener.getLastMessage());
		return result;
	}
	
}
