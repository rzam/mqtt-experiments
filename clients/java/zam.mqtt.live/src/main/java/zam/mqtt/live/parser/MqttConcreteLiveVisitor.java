package zam.mqtt.live.parser;

import org.antlr.v4.runtime.misc.NotNull;

public class MqttConcreteLiveVisitor extends MqttLiveBaseVisitor<ParseData> {

	@Override
	public ParseData visitUnsubscribeExpression(
			@NotNull MqttLiveParser.UnsubscribeExpressionContext ctx) {
		ParseData result = new ParseData(ParseData.Type.UNSUBSCRIBE);
		result.setString("topic",stripString(ctx.topic.getText()));
		return result;
	}

	@Override
	public ParseData visitConnectExpression(
			@NotNull MqttLiveParser.ConnectExpressionContext ctx) {
		ParseData result = new ParseData(ParseData.Type.CONNECT);
		result.setString("host",stripString(ctx.host.getText()));
		result.setInt("port",Integer.parseInt(ctx.port.getText()));
		result.setString("id",stripString(ctx.id.getText()));
		if (ctx.user != null) {
			result.setString("user",stripString(ctx.user.getText()));
			result.setString("password",stripString(ctx.password.getText()));
		}
		return result;
	}

	@Override
	public ParseData visitDisconnectExpression(
			@NotNull MqttLiveParser.DisconnectExpressionContext ctx) {
		return new ParseData(ParseData.Type.DISCONNECT);
	}

	@Override
	public ParseData visitQuitExpression(
			@NotNull MqttLiveParser.QuitExpressionContext ctx) {
		return new ParseData(ParseData.Type.QUIT);
	}

	@Override
	public ParseData visitHelpExpression(
			@NotNull MqttLiveParser.HelpExpressionContext ctx) {
		return new ParseData(ParseData.Type.HELP);
	}

	@Override
	public ParseData visitPublishExpression(
			@NotNull MqttLiveParser.PublishExpressionContext ctx) {
		ParseData result = new ParseData(ParseData.Type.PUBLISH);
		result.setString("message",stripString(ctx.message.getText()));
		result.setString("topic",stripString(ctx.topic.getText()));
		result.setInt("qos",ctx.qos == null ? 0 : Integer.parseInt(ctx.qos.getText()));
		return result;
	}
	
	@Override
	public ParseData visitSubscribeExpression(
			@NotNull MqttLiveParser.SubscribeExpressionContext ctx) {
		ParseData result = new ParseData(ParseData.Type.SUBSCRIBE);
		result.setString("topic",stripString(ctx.topic.getText()));
		result.setInt("qos",ctx.qos == null ? 0 : Integer.parseInt(ctx.qos.getText()));
		return result;
	}
	
	private String stripString(String string) {
		return string.substring(1,string.length()-1);
	}

}
