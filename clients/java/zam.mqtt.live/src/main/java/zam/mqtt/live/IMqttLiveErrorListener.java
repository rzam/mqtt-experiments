package zam.mqtt.live;

public interface IMqttLiveErrorListener {

	void onError(String cause);
	
}
