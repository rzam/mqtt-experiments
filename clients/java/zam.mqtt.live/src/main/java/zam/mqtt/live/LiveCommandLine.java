package zam.mqtt.live;

import java.util.Scanner;

import zam.mqtt.live.parser.MqttParser;
import zam.mqtt.live.parser.ParseData;

public class LiveCommandLine {

	private static Scanner scanner = new Scanner(System.in);
	
	private static String getMessage() {
		System.out.print("> ");
		String line = null;
		try {
			line = scanner.nextLine();
			if (line.length() == 0) return null;
		} catch (Exception e) { }
		return line;
	}
	
	public static void main(String[] args) {
		
		MqttLiveClient client = new MqttLiveClient();
		MqttParser parser = new MqttParser();
		String message;
		ParseData data;
		
		client.setMessageListener(new IMqttLiveMessageListener() {
			@Override public void onMessage(String message, String topic) {
				if (topic != null)
					System.out.println("# Received message \"" +
						message + "\" on topic \"" + topic + "\"");
				else
					System.out.println("* Info: " + message);
			}
		});
		
		client.setErrorListener(new IMqttLiveErrorListener() {
			@Override public void onError(String cause) {
				System.out.println("! Error: " + cause);
			}
		});
		
		do {
			message = getMessage();
			if (message == null) break;
			try { data = parser.parse(message); }
			catch (RuntimeException e) {
				System.err.println(e.getMessage());
				continue;
			}
			
			switch (data.getType()) {
				case CONNECT:
					if (data.getString("user") != null)
						client.connect(data.getString("host"),data.getInt("port"),data.getString("id"),
							data.getString("user"),data.getString("password"));
					else
						client.connect(data.getString("host"),data.getInt("port"),data.getString("id"));
					break;
				case DISCONNECT:
					client.disconnect();
					break;
				case PUBLISH:
					client.publish(data.getString("message"),data.getString("topic"), data.getInt("qos"));
					break;
				case SUBSCRIBE:
					client.subscribe(data.getString("topic"), data.getInt("qos"));
					break;
				case UNSUBSCRIBE:
					client.unsubscribe(data.getString("topic"));
					break;
				case QUIT:
					client.quit();
					break;
				case HELP:
					client.help();
					break;
			}
			
		} while (true);

	}

}
