package zam.mqtt.live.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ParseData {
	
	public enum Type {
		CONNECT, DISCONNECT,
		PUBLISH,
		SUBSCRIBE, UNSUBSCRIBE,
		QUIT, HELP
	};
	
	private ParseData.Type type;
	private Map<String,String> strings;
	private Map<String,Integer> numbers;
	
	public ParseData(ParseData.Type type) {
		this.type = type;
		this.strings = new HashMap<String,String>();
		this.numbers = new HashMap<String,Integer>();
	}
	
	public ParseData.Type getType() {
		return type;
	}
	
	public String getString(String name) {
		return strings.containsKey(name) ? strings.get(name) : null;
	}
	public void setString(String name, String value) {
		strings.put(name,value);
	}
	
	public int getInt(String name) {
		return numbers.containsKey(name) ? numbers.get(name) : 0;
	}
	public void setInt(String name, int value) {
		numbers.put(name,value);
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(type.toString());
		builder.append(" { ");
		ArrayList<String> values = new ArrayList<String>(); 
		for (String key : strings.keySet())
			values.add(key + ":\"" + strings.get(key) + "\""); 
		for (String key : numbers.keySet())
			values.add(key + ":" + numbers.get(key));
		for (int i=0;i<values.size();++i) {
			builder.append(values.get(i));
			if (i < values.size()-1) builder.append(", ");
		}
		builder.append(" }");
		return builder.toString();
	}

}
