package zam.mqtt.live;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class MqttLiveClient implements MqttCallback {

	private IMqttLiveMessageListener messageListener;
	private IMqttLiveErrorListener errorListener;
	
	private MqttClient client;
	
	/***** Client control *****/	
	public void connect(String host, int port, String id, String username, String password) {
		if (client != null) notifyError("Already connected to a broker");
		else {
			String address = "tcp://" + host + ":" + port;
			try {
				client = new MqttClient(address,id);
				MqttConnectOptions options = new MqttConnectOptions();
				if (username != null) options.setUserName(username);
				if (password != null) options.setPassword(password.toCharArray());
				options.setCleanSession(true);
				client.connect(options);
				client.setCallback(this);
				notifyInfo("Successfully connected to the broker.");
			} catch (MqttException e) {
				notifyError("Couldn't connect to the broker: " + e.getMessage());
				client = null;
			}
		}
	}

	public void connect(String host, int port, String id) {
		connect(host,port,id,null,null);
	}
	
	public void disconnect() {
		if (client == null) notifyError("Not connected to any broker");
		else {
			try {
				client.disconnect();
				client.close();
				notifyInfo("Successfully disconnected from the broker.");
			} catch (MqttException e) {
				notifyError("Error while disconnecting from the broker: " + e.getCause());
			}
			finally { client = null; }
		}
	}
	
	public void publish(String message, String topic, int qos) {
		if (client == null) notifyError("Not connected to any broker");
		else {
			MqttMessage mqttMessage = new MqttMessage(message.getBytes());
			mqttMessage.setQos(qos);
			try {
				client.publish(topic,mqttMessage);
				notifyInfo("Successfully published the message.");
			} catch (MqttException e) {
				notifyError("Couldn't publish the message: " + e.getCause());
			}
		}
	}
	
	public void subscribe(String topic, int qos) {
		if (client == null) notifyError("Not connected to any broker");
		else {
			try {
				client.subscribe(topic,qos);
				notifyInfo("Successfully subscribed to the topic.");
			} catch (MqttException e) {
				e.printStackTrace();
				notifyError("Couldn't subscribe to the topic: " + e.getCause());
			}
		}
	}
	
	public void unsubscribe(String topic) {
		if (client == null) notifyError("Not connected to any broker");
		else {
			try {
				client.unsubscribe(topic);
				notifyInfo("Successfully unsubscribed from the topic.");
			} catch (MqttException e) {
				notifyError("Error while unsubscribing from the topic: " + e.getCause());
			}
		}
	}
	
	public void quit() {
		if (client != null) disconnect();
		System.exit(0);
	}
	
	public void help() {
		notifyInfo("Available commands:\n"
			+ "** connect to HOST on port PORT with id ID (as user USER with password PASSWORD)\n"
			+ "** disconnect\n"
			+ "** publish MESSAGE on TOPIC with qos QOS\n"
			+ "** subscribe to TOPIC with qos QOS\n"
			+ "** unsubscribe from TOPIC\n"
			+ "** quit\n"
			+ "** help\n"
		);
	}
	
	/***** Listener control *****/
	public void setMessageListener(IMqttLiveMessageListener listener) {
		messageListener = listener;
	}
	public void clearMessageListener() {
		messageListener = null;
	}
	
	public void setErrorListener(IMqttLiveErrorListener listener) {
		errorListener = listener;
	}
	public void clearErrorListener() {
		errorListener = null;
	}
	
	private void notifyMessage(String message, String topic) {
		if (messageListener != null)
			messageListener.onMessage(message,topic);
	}
	private void notifyInfo(String message) {
		notifyMessage(message,null);
	}
	private void notifyError(String message) {
		if (errorListener != null)
			errorListener.onError(message);
	}

	/***** Callback *****/
	@Override
	public void connectionLost(Throwable cause) {
		notifyError("Connection to the broker lost");
		client = null;
	}

	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		notifyMessage(new String(message.getPayload()),topic);
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token) { }
	
}