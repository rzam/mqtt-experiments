package zam.mqtt.live;

public interface IMqttLiveMessageListener {
	
	void onMessage(String message, String topic);

}
