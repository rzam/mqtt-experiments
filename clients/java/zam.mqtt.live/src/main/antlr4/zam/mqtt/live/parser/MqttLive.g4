grammar MqttLive;

root: (
    connectExpression
    | disconnectExpression
    | publishExpression
    | subscribeExpression
    | unsubscribeExpression
    | quitExpression
    | helpExpression
)*;

connectExpression:
    'connect' 'to' host=STRING ('on'|'using'|'with') 'port' port=INTEGER ('with'|'using') 'id' id=STRING
    ('as' 'user' user=STRING 'with' 'password' password=STRING)?
;

disconnectExpression:
    'disconnect'
;

publishExpression:
    'publish' message=STRING ('to'|'on') 'topic' topic=STRING ('with' 'qos' qos=('1'|'2'|'3'))?
;

subscribeExpression:
    'subscribe' ('to'|'on') 'topic' topic=STRING ('with' 'qos' qos=('1'|'2'|'3'))?
;

unsubscribeExpression:
    'unsubscribe' 'from' 'topic' topic=STRING
;

quitExpression:
    'quit'
;

helpExpression:
    'help'
;

INTEGER: [0-9]+;
STRING: '"' (~[\r\n"] | '""')* '"';
WS: [ \n\t\r]+ -> skip;
