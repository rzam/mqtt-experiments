package zam.mqtt.android;

import java.util.ArrayList;
import java.util.List;

import zam.mqtt.android.ui.SensorAdapter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity
	implements OnItemClickListener, SensorEventListener, OnSeekBarChangeListener {
	
	private List<SensorWrapper> sensors = new ArrayList<SensorWrapper>();;
	private ListView sensorList;
	private SensorAdapter adapter;
    private SensorManager manager;
    private Button toggleButton;
    
    private String host, clientID, user, password;
    private int port, rate = SensorManager.SENSOR_DELAY_NORMAL;
    
    private EditText hostEdit, portEdit, idEdit, userEdit, passwordEdit;
    private TextView lastMessage;
    
    private boolean sensorsEnabled = false;
    
    private MqttHelper helper = new MqttHelper(this);
    private SharedPreferences preferences;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);	
		preferences = PreferenceManager.getDefaultSharedPreferences(this);
		manager = (SensorManager)getSystemService(SENSOR_SERVICE);
		initialize();
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		listAvailableSensors();
	}
	
	@Override
	protected void onPause() {
		saveEditTextValues();
		unregisterAllSensors();
		helper.disconnect();
		super.onPause();
	}
	
	@Override
	protected void onResume() {
		loadEditTextValues();
		registerAllSensors();
		super.onResume();
	}
	
	@Override
	protected void onSaveInstanceState(Bundle bundle) {
		bundle.putString("host",hostEdit.getText().toString());
		bundle.putString("port",portEdit.getText().toString());
		bundle.putString("id",idEdit.getText().toString());
		bundle.putString("user",userEdit.getText().toString());
		bundle.putString("password",passwordEdit.getText().toString());
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle bundle) {
		hostEdit.setText(bundle.getString("host"));
		portEdit.setText(bundle.getString("port"));
		idEdit.setText(bundle.getString("id"));
		userEdit.setText(bundle.getString("user"));
		passwordEdit.setText(bundle.getString("password"));
	}
	
	public void displayMessage(final String message) {
		if (Looper.getMainLooper().equals(Looper.myLooper())) {
			Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
			lastMessage.setText(message);
		} else {
			runOnUiThread(new Runnable(){
				@Override public void run() { displayMessage(message); }
			});
		}
	}
	
	/***** Initialization *****/
	private void initialize() {
		sensorList   = (ListView)findViewById(R.id.sensorList);
		toggleButton = (Button)findViewById(R.id.toggleButton);
		hostEdit     = (EditText)findViewById(R.id.hostEdit);
		portEdit     = (EditText)findViewById(R.id.portEdit);
		idEdit       = (EditText)findViewById(R.id.idEdit);
		userEdit     = (EditText)findViewById(R.id.userEdit);
		passwordEdit = (EditText)findViewById(R.id.passwordEdit);
		lastMessage  = (TextView)findViewById(R.id.lastMessage);
		sensorList.setOnItemClickListener(this);
		((SeekBar)findViewById(R.id.rateSeekbar)).setOnSeekBarChangeListener(this);
		EditText[] targets = new EditText[]{ hostEdit, portEdit, idEdit, userEdit, passwordEdit };
		for (EditText target : targets)
			target.addTextChangedListener(new TargetedTextWatcher(target.getId()));
	}
	
	/***** Preferences *****/
	private void loadEditTextValues() {
		hostEdit.setText(preferences.getString("host","192.168.1.29"));
		portEdit.setText(preferences.getString("port","1883"));
		idEdit.setText(preferences.getString("id","test"));
		userEdit.setText(preferences.getString("user","user"));
		passwordEdit.setText(preferences.getString("password","password"));
	}
	
	private void saveEditTextValues() {
		Editor editor = preferences.edit();
		editor.putString("host",hostEdit.getText().toString());
		editor.putString("port",portEdit.getText().toString());
		editor.putString("id",idEdit.getText().toString());
		editor.putString("user",userEdit.getText().toString());
		editor.putString("password",passwordEdit.getText().toString());
		editor.apply();
	}

	/***** Sensor management *****/
	private void listAvailableSensors() {
	    List<Sensor> availableSensors = manager.getSensorList(Sensor.TYPE_ALL);
	    sensors.clear();
	    adapter = new SensorAdapter(this,sensors);
	    sensorList.setAdapter(adapter);
	    for (Sensor sensor : availableSensors) sensors.add(new SensorWrapper(sensor));
	    adapter.notifyDataSetInvalidated();
	}
	
	private void registerAllSensors() {
		for (SensorWrapper wrapper : sensors) {
			if (wrapper.isEnabled())
				manager.registerListener(this,wrapper.getSensor(),rate);
		}
	}
	
	private void unregisterAllSensors() {
		for (SensorWrapper wrapper : sensors) {
			if (wrapper.isEnabled())
				manager.unregisterListener(this,wrapper.getSensor());
		}
	}
	
	@Override
	public void onSensorChanged(SensorEvent event) {
		helper.processSensorEvent(event);
	}
	
	@Override public void onAccuracyChanged(Sensor sensor, int accuracy) { }

	
	/***** UI callbacks *****/
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		SensorWrapper wrapper = sensors.get(position);
		wrapper.toggleEnabled();
	    adapter.notifyDataSetChanged();
	    if (!sensorsEnabled) return;
		if (wrapper.isEnabled()) manager.registerListener(this,wrapper.getSensor(),rate);
		else  manager.unregisterListener(this,wrapper.getSensor());
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
		rate = progress == 0 ? SensorManager.SENSOR_DELAY_NORMAL :
			   progress == 1 ? SensorManager.SENSOR_DELAY_UI     :
			   progress == 2 ? SensorManager.SENSOR_DELAY_GAME   :
			                   SensorManager.SENSOR_DELAY_FASTEST;
	}
	
	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		unregisterAllSensors();
		if (sensorsEnabled) registerAllSensors();
	}

	@Override public void onStartTrackingTouch(SeekBar seekBar) { }
	
	public void buttonClick(View view) {
		switch (view.getId()) {
			case R.id.connectButton:
				helper.connect(host,port,clientID,user,password);
				break;
			case R.id.disconnectButton:
				helper.disconnect();
				break;
			case R.id.toggleButton:
				toggleEnable();
		}
	}
	
	private void toggleEnable() {
		sensorsEnabled = !sensorsEnabled;
		if (sensorsEnabled) {
			registerAllSensors();
			toggleButton.setText(R.string.toggle_button_disable);
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		} else {
			unregisterAllSensors();
			toggleButton.setText(R.string.toggle_button_enable);
			getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		}
	}
	
	/***** Custom TextWatcher *****/
	private class TargetedTextWatcher implements TextWatcher {

	    private int id;
	    
	    private TargetedTextWatcher(int id) {
	    	this.id = id;
	    }

	    public void afterTextChanged(Editable editable) {
	        String text = editable.toString();
	        switch (id) {
	        	case R.id.hostEdit:
	        		host = text;
	        		break;
	        	case R.id.portEdit:
	        		try { port = Integer.parseInt(text); }
	        		catch (NumberFormatException nfe) { port = 1883; }
	        		break;
	        	case R.id.idEdit:
	        		clientID = text;
	        		break;
	        	case R.id.userEdit:
	        		user = text;
	        		break;
	        	case R.id.passwordEdit:
	        		password = text;
	        }
	    }
	    
		@Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
		@Override public void onTextChanged(CharSequence s, int start, int before, int count) { }

	    
	}
	
}
