package zam.mqtt.android;

import android.hardware.Sensor;

public class SensorWrapper {

	private Sensor sensor;
	private boolean enabled;
	
	public SensorWrapper(Sensor sensor) {
		this.sensor = sensor;
	}
	
	
	public Sensor getSensor() {
		return sensor;
	}

	public void setSensor(Sensor sensor) {
		this.sensor = sensor;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void toggleEnabled() {
		this.enabled = !enabled;
	}

}
