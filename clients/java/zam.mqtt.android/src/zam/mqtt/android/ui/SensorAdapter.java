package zam.mqtt.android.ui;

import java.util.List;

import zam.mqtt.android.R;
import zam.mqtt.android.SensorWrapper;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class SensorAdapter extends ArrayAdapter<SensorWrapper> {
	
	private Context context;
	private List<SensorWrapper> objects;

	public SensorAdapter(Context context, List<SensorWrapper> objects) {
		super(context,R.layout.sensor_adapter_element,objects);
		this.context = context;
		this.objects = objects;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		 if (convertView==null) {
			 LayoutInflater inflater = ((Activity)context).getLayoutInflater();
			 convertView = inflater.inflate(R.layout.sensor_adapter_element,parent,false);
		 }
		 SensorWrapper wrapper = objects.get(position);
		 Sensor sensor = wrapper.getSensor();
		 ((TextView)convertView.findViewById(R.id.sensor)).setText(sensor.getName()); 
		 if (wrapper.isEnabled())
			 convertView.setBackgroundColor(context.getResources().getColor(R.color.selected_sensor));
		 else
			 convertView.setBackgroundColor(Color.TRANSPARENT);
		 return convertView;
	}


}
