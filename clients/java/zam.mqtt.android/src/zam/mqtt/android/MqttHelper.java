package zam.mqtt.android;

import java.util.Arrays;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClientPersistence;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import android.hardware.SensorEvent;

public class MqttHelper implements IMqttActionListener, MqttCallback {

	private enum Action { CONNECT, DISCONNECT, PUBLISH };
	
	private MainActivity parent;
	private MqttAsyncClient client;
	private MqttMessage message;
	private MqttClientPersistence persistence;

	public MqttHelper(MainActivity parent) {
		this.parent = parent;
		this.message = new MqttMessage();
		message.setQos(0);
		this.persistence = new MemoryPersistence();
	}
	
	public void connect(String host, int port, String id) {
		connect(host,port,id,null,null);
	}
	
	public void connect(String host, int port, String id, String user, String password) {
		if (client != null) {
			parent.displayMessage("Already connected");
			return;
		}
		String address = "tcp://" + host + ":" + port;
		try {
			client = new MqttAsyncClient(address,id,persistence);
			client.setCallback(this);
			MqttConnectOptions options = new MqttConnectOptions();
			if (user != null && user.length() > 0)
				options.setUserName(user);
			if (password != null && password.length() > 0)
				options.setPassword(password.toCharArray());
			options.setConnectionTimeout(5);
			client.connect(options,Action.CONNECT,this);
		} catch (MqttException e) { e.printStackTrace(); }
	}
	
	public void disconnect() {
		if (client == null) {
			parent.displayMessage("Not connected");
			return;
		}
		try { client.disconnect(Action.DISCONNECT,this); }
		catch (MqttException e) { e.printStackTrace(); }
	}
	
	public void processSensorEvent(SensorEvent event) {
		if (client == null || !client.isConnected()) return;
		String data = event.timestamp + "," + Arrays.toString(event.values);
		message.setPayload(data.getBytes());
		try { client.publish("android/sensors/" + event.sensor.getName(),message); }
		catch (MqttException e) { e.printStackTrace(); }
	}

	@Override
	public void onSuccess(IMqttToken asyncActionToken) {
		Action action = (Action)asyncActionToken.getUserContext();
		if (action.equals(Action.CONNECT)) parent.displayMessage("Connected");
		else if (action.equals(Action.DISCONNECT)) {
			parent.displayMessage("Disconnected");
			client = null;
		}
	}

	@Override
	public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
		parent.displayMessage(exception.getCause().getMessage());
		client = null;
	}

	@Override
	public void connectionLost(Throwable cause) {
		parent.displayMessage("Connection lost");
		client = null;
	}

	@Override public void messageArrived(String topic, MqttMessage message) throws Exception { }
	@Override public void deliveryComplete(IMqttDeliveryToken token) { }

}
