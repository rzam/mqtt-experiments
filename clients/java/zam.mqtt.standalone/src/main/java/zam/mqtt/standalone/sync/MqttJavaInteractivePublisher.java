package zam.mqtt.standalone.sync;
import java.util.Scanner;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import zam.mqtt.standalone.OptionManager;

public class MqttJavaInteractivePublisher { 
	
	private static Scanner scanner = new Scanner(System.in);
	
	private static String getMessage() {
		System.out.print("> ");
		String line = scanner.nextLine();
		if (line.length() == 0) return null;
		return line;
	}
	
	public static void main(String[] args) {
    	
    	OptionManager manager = new OptionManager();
    	CommandLine line = null;
    	try {
    		line = manager.parseOptions(args);
    	} catch (ParseException e) {
			System.err.println("Error: " + e.getMessage());
			manager.printHelp("MqttJavaPublisher OPTIONS MESSAGE");
			System.exit(1);
		}
		
		String topic   = (String)line.getOptionObject('t');
        String broker  = (String)line.getOptionObject('b');
        String id      = (String)line.getOptionObject('i');
        int qos        = ((Long)line.getOptionObject('q')).intValue();
        
        boolean debug  = line.hasOption('d');
        
        try {
        	
        	MqttClient client = new MqttClient(broker,id);
            MqttConnectOptions mqttOptions = new MqttConnectOptions();
            if (line.hasOption("u")) mqttOptions.setUserName(line.getOptionValue("u"));
            if (line.hasOption("p")) mqttOptions.setPassword(line.getOptionValue("p").toCharArray());
            mqttOptions.setCleanSession(line.hasOption("c"));
            
            if (debug) System.out.print("Connecting to broker " + broker + "... ");
            client.connect(mqttOptions);
            if (debug) System.out.println("connected.");
            
            do {
            	String message = getMessage();
            	if (message == null) break;
            	if (debug) System.out.print("Publishing message " + message + "... ");
            	MqttMessage mqttMessage = new MqttMessage(message.getBytes());
            	mqttMessage.setQos(qos);
            	client.publish(topic,mqttMessage);
            	if (debug) System.out.println("message published.");
            } while (true);

            if (debug) System.out.print("Disconnecting from broker... ");
            client.disconnect();
            if (debug) System.out.println("disconnected.");
            
            System.exit(0);
            
        } catch(MqttException me) {
        	System.err.println("Couldn't publish the message.");
            System.err.println("Reason: " + me.getReasonCode());
            System.err.println("Message: " + me.getMessage());
            System.err.println("Cause: " + me.getCause());
        }
    }
}