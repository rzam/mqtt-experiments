package zam.mqtt.standalone;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class OptionManager {
	
	private Options options = null;
	
	public OptionManager() {
		buildOptions();
	}
	
	@SuppressWarnings("static-access")
	private Option createOption(char letter, String desc, String arg, Object type, boolean required) {
		OptionBuilder
				.hasArg()
				.withArgName(arg)
				.withDescription(desc)
				.withType(type);
		if (required) OptionBuilder.isRequired();
		return OptionBuilder.create(letter);
	}

    @SuppressWarnings("static-access")
	public void buildOptions() {
		options = new Options();
		options.addOption(createOption('t',"topic name","TOPIC",String.class,true));
		options.addOption(createOption('q',"message QoS","QoS",Number.class,true));
		options.addOption(createOption('b',"broker address","ADDRESS",String.class,true));
		options.addOption(createOption('i',"client id","ID",String.class,true));
		options.addOption(createOption('u',"username","USERNAME",String.class,false));
		options.addOption(createOption('p',"password","PASSWORD",String.class,false));
		options.addOption(OptionBuilder.withDescription("clean session").create('c'));
		options.addOption(OptionBuilder.withDescription("print debug message").create('d'));
    }
	
	public CommandLine parseOptions(String[] args) throws ParseException {
		GnuParser parser = new GnuParser();
		CommandLine line = parser.parse(options,args);
		return line;
    }
    
    public void printHelp(String usage) {
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp(usage,options);
    }
		
}
