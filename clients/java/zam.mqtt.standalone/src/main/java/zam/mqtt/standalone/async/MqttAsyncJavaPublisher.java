package zam.mqtt.standalone.async;

import java.util.concurrent.Semaphore;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import zam.mqtt.standalone.OptionManager;

public class MqttAsyncJavaPublisher implements IMqttActionListener {
	
	private String topic;
	private String broker;
    private String id;
    private int qos;
    private String message;
    private boolean debug;
    
	private MqttAsyncClient client;
    
    private IMqttToken connectToken;
    private IMqttToken publishToken;
    
    private Semaphore semaphore;
	
	public MqttAsyncJavaPublisher(CommandLine line) {
		
		topic   = (String)line.getOptionObject('t');
        broker  = (String)line.getOptionObject('b');
        id      = (String)line.getOptionObject('i');
        qos     = ((Long)line.getOptionObject('q')).intValue();
        message = line.getArgs()[0];
        
        debug     = line.hasOption('d');
        semaphore = new Semaphore(0);
        
        try {
        	
        	client = new MqttAsyncClient(broker,id);
        	
        	MqttConnectOptions mqttOptions = new MqttConnectOptions();
            if (line.hasOption("u")) mqttOptions.setUserName(line.getOptionValue("u"));
            if (line.hasOption("p")) mqttOptions.setPassword(line.getOptionValue("p").toCharArray());
            mqttOptions.setCleanSession(line.hasOption("c"));
            
            if (debug) System.out.print("Connecting to broker " + broker + "... ");
            connectToken = client.connect(mqttOptions,null,this);
            
            semaphore.acquire();
            client.disconnect();
            System.out.println("disconnected.");
            
            System.exit(0);
        	
            
        }
        catch(MqttException me) { me.printStackTrace(); }
        catch (InterruptedException e) { e.printStackTrace(); }
    }
	
	@Override
	public void onSuccess(IMqttToken asyncActionToken) {
		if (asyncActionToken.equals(connectToken)) {
            if (debug) System.out.println("connected.");
            MqttMessage mqttMessage = new MqttMessage(message.getBytes());
            mqttMessage.setQos(qos);
            if (debug) System.out.print("Publishing message " + message + "... ");
			try { publishToken = client.publish(topic,mqttMessage,null,this); }
			catch (MqttException e) { }
		} else if (asyncActionToken.equals(publishToken)) {
			if (debug) System.out.println("message published.");
            if (debug) System.out.print("Disconnecting from broker... ");
			semaphore.release();
		}
	}

	@Override
	public void onFailure(IMqttToken asyncActionToken, Throwable e) {
    	System.err.println("Couldn't publish the message.");
        System.err.println("Reason: " + e.getMessage());
	}
	
	public static void main(String[] args) {
		OptionManager manager = new OptionManager();
		CommandLine line = null;
		try {
			line = manager.parseOptions(args);
			if (line.getArgs().length == 0)
				throw new ParseException("no message provided");
		} catch (ParseException e) {
			System.err.println("Error: " + e.getMessage());
			manager.printHelp("MqttAsyncJavaPublisher OPTIONS MESSAGE");
			System.exit(1);
		}
		new MqttAsyncJavaPublisher(line);
	}

}