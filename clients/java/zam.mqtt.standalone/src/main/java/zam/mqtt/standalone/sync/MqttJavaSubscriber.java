package zam.mqtt.standalone.sync;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import zam.mqtt.standalone.OptionManager;

public class MqttJavaSubscriber { 

	public static void main(String[] args) {
	
    	OptionManager manager = new OptionManager();
    	CommandLine line = null;
    	try { line = manager.parseOptions(args); }
    	catch (ParseException e) {
			System.err.println("Error: " + e.getMessage());
			manager.printHelp("MqttJavaSubscriber OPTIONS");
			System.exit(1);
		}
		
		String topic   = (String)line.getOptionObject('t');
        String broker  = (String)line.getOptionObject('b');
        String id      = (String)line.getOptionObject('i');
        int qos        = ((Long)line.getOptionObject('q')).intValue();
        
        boolean debug  = line.hasOption('d');
        
        try {
        	
        	MqttClient client = new MqttClient(broker,id);
            MqttConnectOptions mqttOptions = new MqttConnectOptions();
            if (line.hasOption("u")) mqttOptions.setUserName(line.getOptionValue("u"));
            if (line.hasOption("p")) mqttOptions.setPassword(line.getOptionValue("p").toCharArray());
            mqttOptions.setCleanSession(line.hasOption("c"));
            
            if (debug) System.out.print("Connecting to broker " + broker + "... ");
            client.connect(mqttOptions);
            if (debug) System.out.println("connected.");
        
            if (debug) System.out.print("Subscribing to topic " + topic + "... ");
            client.setCallback(new MqttSubscribeCallback(debug));
            client.subscribe(topic,qos);
            if (debug) System.out.println("subscribed.");
            
        } catch(MqttException me) {
        	System.err.println("Couldn't subscribe to topic.");
            System.err.println("Reason: " + me.getReasonCode());
            System.err.println("Message: " + me.getMessage());
            System.err.println("Cause: " + me.getCause());
        }
    }
}

class MqttSubscribeCallback implements MqttCallback {
	
	private boolean debug;
	
	public MqttSubscribeCallback(boolean debug) { this.debug = debug; }

	@Override
	public void connectionLost(Throwable cause) {
		if (debug) System.err.println("Connection lost: " + cause.getMessage());
	}

	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		System.err.println("[" + topic + "] " + message);
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		if (debug) System.out.println("Delivery complete.");
	}
	
}