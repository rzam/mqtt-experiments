Basic Configuration
===================

* Start:
    `mosquitto -c mosquitto.conf`
* Start (daemon):
    `mosquitto -c mosquitto.conf -d`
* Adding users:
    `mosquitto -U mosquitto.passwd USERNAME`


SSL/TLS Configuration
=====================

Generate a self-signed root CA and its private key (used to sign both server and client certificates):

```
openssl req -new -x509 -days 365 -extensions v3_ca -keyout ca.key -out ca.crt
```

Server Configuration
--------------------

Generate a private key for the server:

```
openssl genrsa -des3 -out server.key 2048
```

Generate the server certificate using the server private key:

```
openssl req -out server.csr -key server.key -new
```

Sign the server certificate with the self-signed root CA:

```
openssl x509 -req -in server.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out server.crt -days 365
```

Client Configuration
--------------------

Generate a private key for the client:

```
openssl genrsa -des3 -out client.key 2048
```

Generate the client certificate using the client private key:

```
openssl req -out client.csr -key client.key -new
```

Sign the client certificate with the self-signed root CA:

```
openssl x509 -req -in client.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out client.crt -days 365
```


Testing
=======

* Publisher:
    `mosquitto_pub -v -d [-h HOST] [-p PORT] [-i ID] [-u USER] [-P PASSWORD] [-q QOS] -t TOPIC -m MESSAGE`
* Subscriber:
    `mosquitto_sub -v -d [-h HOST] [-p PORT] [-i ID] [-u USER] [-P PASSWORD] [-q QOS] -t TOPIC`
* Publisher (certificate):
    `mosquitto_pub -v -d [-h HOST] [-p PORT] [-i ID] [-u USER] [-P PASSWORD] [-q QOS] -t TOPIC -m MESSAGE --cafile CAFILE --cert CLIENTCERT --key CLIENTKEY`
* Subscriber (certificate):
    `mosquitto_sub -v -d [-h HOST] [-p PORT] [-i ID] [-u USER] [-P PASSWORD] [-q QOS] -t TOPIC --cafile CAFILE --cert SERVERCERT --key SERVERKEY`
* Publisher (psk):
    `mosquitto_pub -v -d [-h HOST] [-p PORT] [-i ID] [-u USER] [-P PASSWORD] [-q QOS] -t TOPIC -m MESSAGE --psk-identity ID --psk PSK`
* Subscriber (psk):
    `mosquitto_sub -v -d [-h HOST] [-p PORT] [-i ID] [-u USER] [-P PASSWORD] [-q QOS] -t TOPIC --psk-identity ID --psk PSK`
