Configuration
=============

Enable the MQTT plugin:

```
rabbitmq-plugins enable rabbitmq_mqtt
```

Set RABBITMQ_CONFIG_FILE to point to the configuration file:

```
export RABBITMQ_CONFIG_FILE=/etc/rabbitmq/rabbitmq.config
```

Add an exchange to be used by the MQTT plugin if necessary.

Add a new MQTT user if necessary:

```
rabbitmqctl add_user USER PASSWORD
rabbitmqctl set_permissions -p / USER ".*" ".*" ".*"
```
